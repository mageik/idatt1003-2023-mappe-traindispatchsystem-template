package edu.ntnu.stud.kandidat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.stud.kandidat.register.TrainDeparture;
import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** Test class for {@link TrainDeparture} functionality */
@DisplayName("Test of class")
class TrainDepartureTest {
  LocalTime originalDepartureTime;
  String line;
  String destination;
  int trainTrack;
  Duration testDelay;

  /** Set-up for initializing useful variables for testing. */
  @BeforeEach
  void setUp() {
    originalDepartureTime = LocalTime.of(14, 0);
    line = "G7";
    destination = "Oslo";
    trainTrack = 14;
    testDelay = Duration.ofMinutes(30);
  }

  /**
   * Tests that check if verification methods in {@link TrainDeparture} throws exceptions for
   * incorrect parameters.
   */
  @Nested
  @DisplayName("Unit tests for TrainDeparture methods which do throw exceptions")
  public class ExceptionsThrown {
    @Test
    @DisplayName("Tests if the TrainDeparture constructor throws exception for null departure time")
    public void trainDepartureTestConstructorLocalTime() {
      try {
        new TrainDeparture(null, line, destination, trainTrack);
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Invalid time input. Time cannot be null", e.getMessage());
      }
    }

    /** Tests that TrainDeparture throws exceptions for blank line parameter. */
    @Test
    @DisplayName("Tests if the TrainDeparture constructor throws exception for blank line")
    public void trainDepartureTestConstructorLine() {
      try {
        new TrainDeparture(originalDepartureTime, "", destination, trainTrack);
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Train Line is an empty string", e.getMessage());
      }
    }

    /**
     * Tests that TrainDeparture constructor throws IllegalArgumentException for blank parameter.
     */
    @Test
    @DisplayName("Tests if the TrainDeparture constructor throws exception for blank destination")
    public void trainDepartureTestConstructorDestination() {
      try {
        new TrainDeparture(originalDepartureTime, line, "", trainTrack);
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Destination is an empty string", e.getMessage());
      }
    }

    /** Tests that addMinuteDelay throws IllegalArgumentException for negative parameter. */
    @Test
    @DisplayName("Checks addMinuteDelay. Throws exception if input is a negative int")
    public void setDelayNegative() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        testTrain.addMinuteDelay(Duration.ofMinutes(-1));
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Non-positive duration", e.getMessage());
      }
    }

    /** Checks that setTrack throws exception if input below -1. */
    @Test
    @DisplayName("Checks setTrack. Throws exception if input is lower than -1")
    public void setTrackNegative() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        testTrain.setTrack(-2);
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Please give a positive int or -1 for Train Track", e.getMessage());
      }
    }

    /** Checks that setTrack throws exception if input is 0. */
    @Test
    @DisplayName("Checks setTrack. Throws exception if input is 0")
    public void setTrackZero() {
      try {
        // Arrange
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        // Act
        testTrain.setTrack(0);
        // Assert
        fail("Did not throw exception. Test failed");

      } catch (IllegalArgumentException e) {
        assertEquals("Please give a positive int or -1 for Train Track", e.getMessage());
      }
    }
  }

  /** Class of tests for {@link TrainDeparture} methods that do not throw exceptions. */
  @Nested
  @DisplayName("Unit tests for TrainDeparture methods which do not throw exceptions")
  public class ExceptionsNotThrown {
    /** Test that setTrack correctly sets a new track number. */
    @Test
    @DisplayName("Test that setTrack works")
    public void testSetTrackValid() {
      // Arrange
      TrainDeparture validSetTrackTest =
          new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
      // Act
      validSetTrackTest.setTrack(66);
      // Assert
      assertEquals(66, validSetTrackTest.getTrack());
    }

    /** Test that getDepartureTime returns the correct departure time. */
    @Test
    @DisplayName("Tests that departure time getter returns the correct value")
    public void getDepartureTimeReturnCorrect() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        assertEquals(LocalTime.of(14, 0), testTrain.getDepartureTime());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that getLine returns the correct String. */
    @Test
    @DisplayName("Test that line getter returns the correct line")
    public void getLineCorrectReturn() {
      try {
        // Arrange
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        // Act Assert

        assertEquals(line, testTrain.getLine());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that getDestination returns the correct destination String. */
    @Test
    @DisplayName("Test that train destination getter returns the correct String")
    public void getDestinationCorrectReturn() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);

        assertEquals(destination, testTrain.getDestination());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that getTrack returns the correct int. */
    @Test
    @DisplayName("Test that train track getter returns the correct int")
    public void getTrackCorrectReturn() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);

        assertEquals(trainTrack, testTrain.getTrack());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that getMinuteDelay returns the correct Duration. */
    @Test
    @DisplayName("Test that train delay getter returns the correct Duration")
    public void getDelayCorrectDelay() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);

        testTrain.addMinuteDelay(testDelay);

        assertEquals(testDelay, testTrain.getMinuteDelay());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that addMinuteDelay does not alter originalDepartureTime. */
    @Test
    @DisplayName("Test that train delay setter does not change departure time")
    public void setDelayCorrectDeparture() {
      try {
        TrainDeparture testTrain =
            new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
        testTrain.addMinuteDelay(testDelay);

        assertEquals(originalDepartureTime, testTrain.getOriginalDepartureTime());

      } catch (IllegalArgumentException e) {
        fail("Test failed: " + e.getMessage());
      }
    }

    /** Test that printTrainDeparture prints the correct text if there is a delay. */
    @Test
    @DisplayName("Testing printTrainDeparture prints the correct content with delay")
    void printTrainDepartureTestWithDelay() {
      TrainDeparture printTestTrain = new TrainDeparture(LocalTime.of(6, 15), "B1", "Bergen", 1);
      printTestTrain.addMinuteDelay(Duration.ofMinutes(60));

      String referencePrintActual = printTestTrain.printTrainDeparture(207);
      Duration minuteDelay = Duration.ofMinutes(60);

      String referencePrint =
          String.format(
              "%-15s %-9s %-12s %-15s %-15s %-13d %-2s%n",
              LocalTime.of(6, 15),
              "B1",
              207,
              "Bergen",
              (minuteDelay.isZero()
                  ? ""
                  : (minuteDelay.toMinutes() / 60 + "h, " + minuteDelay.toMinutes() % 60 + "m")),
              1,
              LocalTime.of(7, 15));

      assertEquals(referencePrint, referencePrintActual);
    }

    /** Test that printTrainDeparture prints the correct content if there is no delay. */
    @Test
    @DisplayName("Testing printTrainDeparture prints the correct content without delay")
    void printTrainDepartureTestWithoutDelay() {
      TrainDeparture printTestTrain = new TrainDeparture(LocalTime.of(6, 15), "B1", "Bergen", 1);

      String referencePrintActual = printTestTrain.printTrainDeparture(207);
      Duration minuteDelay = Duration.ofMinutes(0);

      String referencePrint =
          String.format(
              "%-15s %-9s %-12s %-15s %-15s %-13d %-2s%n",
              LocalTime.of(6, 15),
              "B1",
              207,
              "Bergen",
              (minuteDelay.isZero()
                  ? ""
                  : (minuteDelay.toMinutes() / 60 + "h, " + minuteDelay.toMinutes() % 60 + "m")),
              1,
              LocalTime.of(6, 15));

      assertEquals(referencePrint, referencePrintActual);
    }
  }
}
