package edu.ntnu.stud.kandidat;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.kandidat.register.TrainDeparture;
import edu.ntnu.stud.kandidat.register.TrainDepartureRegister;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** Test class of {@link TrainDepartureRegister} functionality */
@DisplayName("Test forTrainDepartureRegister")
public class TrainDepartureRegisterTest {

  private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
  TrainDepartureRegister testTrainDepartureRegister;
  TrainDeparture newTestDeparture;
  TrainDeparture newTestDeparture1;
  TrainDeparture newTestDeparture2;
  LocalTime originalDepartureTime;
  LocalTime originalDepartureTime1;
  LocalTime originalDepartureTime2;
  int trainNumber;
  String line;
  String destination;
  String destination1;

  int trainTrack;
  Duration minuteDelay;

  /**
   * Set-up tests by initializing useful {@link TrainDeparture} objects and a {@link
   * TrainDepartureRegister} object Adds {@link TrainDeparture} objects to {@link
   * TrainDepartureRegister}
   */
  @BeforeEach
  void setUp() {
    System.setOut(new PrintStream(outputStreamCaptor));
    originalDepartureTime = LocalTime.of(12, 0);
    originalDepartureTime1 = LocalTime.of(6, 0);
    originalDepartureTime2 = LocalTime.of(6, 1);
    line = "G5";
    destination = "Oslo";
    destination1 = "Trondheim";
    trainTrack = 3;
    trainNumber = 777;
    minuteDelay = Duration.ofMinutes(30);
    testTrainDepartureRegister = new TrainDepartureRegister();
    newTestDeparture = new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
    newTestDeparture1 =
        new TrainDeparture(originalDepartureTime1, line, destination1, trainTrack + 2);
    newTestDeparture2 =
        new TrainDeparture(originalDepartureTime2, line, destination1, trainTrack + 3);
    testTrainDepartureRegister.addTrainDeparture(newTestDeparture, trainNumber);
    testTrainDepartureRegister.addTrainDeparture(newTestDeparture1, 222);
    testTrainDepartureRegister.addTrainDeparture(newTestDeparture2, 223);
  }

  /** Test that {@link TrainDeparture} objects can be added to a {@link TrainDepartureRegister} */
  @Nested
  @DisplayName("Tests for adding TrainDepartures to TrainDepartureRegister")
  public class AddTrainDepartureRegister {
    @Test
    @DisplayName("Testing new train addition")
    void testTrainDepartureAddition() {
      testTrainDepartureRegister.addTrainDepartureFromInput(
          originalDepartureTime, line, destination, trainTrack, trainNumber + 1);

      assertTrue(testTrainDepartureRegister.trainExists(trainNumber + 1));
    }

    /**
     * Test that two {@link TrainDeparture} objects cannot be added to {@link
     * TrainDepartureRegister} object with the same associated trainNumber as a key in the HashMap.
     */
    @Test
    @DisplayName("Test that one cannot have two train with the same train number")
    void testTrainNumberAlreadyUsed() {
      // Act

      IllegalArgumentException e =
          // Assert
          assertThrows(
              IllegalArgumentException.class,
              () ->
                  testTrainDepartureRegister.addTrainDepartureFromInput(
                      originalDepartureTime, line, destination, trainTrack, trainNumber));
      assertEquals("Train number already in use", e.getMessage());
    }

    /**
     * Test that two {@link TrainDeparture} objects cannot be added to {@link
     * TrainDepartureRegister} object with the same associated trainNumber as a key in the HashMap.
     */
    @Test
    @DisplayName("Test that one cannot have two train with the same train number")
    void testTrainNumberAlreadyUsed2() {
      // Act

      IllegalArgumentException e =
          // Assert
          assertThrows(
              IllegalArgumentException.class,
              () -> testTrainDepartureRegister.addTrainDeparture(newTestDeparture, trainNumber));
      assertEquals("Train number already in use", e.getMessage());
    }
  }

  /** Tests for getter-like methods in {@link TrainDepartureRegister} */
  @Nested
  @DisplayName("Testing getter methods")
  public class getterTests {
    /** Positive test for getter-method with train number as input. */
    @Test
    @DisplayName("Testing getTrainFromTrainNumber for success")
    void testGetTrainFromTrainNumber() {
      // Act
      TrainDeparture actualTrainDeparture =
          testTrainDepartureRegister.getTrainFromTrainNumber(trainNumber);
      // Assert
      assertEquals(
          newTestDeparture,
          actualTrainDeparture,
          "The returned train should be the same as compared");
      assertNotNull(actualTrainDeparture, "Should return train number, not null");
    }

    /** Negative test for getter-method with train number as input. */
    @Test
    @DisplayName("Testing getTrainFromTrainNumber for failure")
    void testGetTrainFromTrainNumberNotFound() {
      TrainDeparture actualTrainDeparture = testTrainDepartureRegister.getTrainFromTrainNumber(69);

      assertNull(
          actualTrainDeparture, "The should be a train with this train number in the register");
    }

    /**
     * Method which checks that method only returns HashMap with trains with the correct
     * destination.
     */
    @Test
    @DisplayName("Testing register only returns departures successfully found by destination")
    void testTrainsFromDestination() {
      // Arrange
      TrainDeparture addedTrain1 =
          new TrainDeparture(originalDepartureTime, line, destination, trainTrack);
      TrainDeparture addedTrain2 =
          new TrainDeparture(originalDepartureTime, line, "New York", trainTrack);
      testTrainDepartureRegister.addTrainDeparture(addedTrain1, trainNumber + 1);
      testTrainDepartureRegister.addTrainDeparture(addedTrain2, trainNumber + 2);
      // Act
      HashMap<Integer, TrainDeparture> outputDepartures =
          testTrainDepartureRegister.getTrainDeparturesFromDestination(destination);
      // Assert
      assertTrue(outputDepartures.containsValue(addedTrain1), "Train not found");
      assertTrue(outputDepartures.containsValue(newTestDeparture), "Train not found");
      assertFalse(outputDepartures.containsValue(addedTrain2), "Incorrect train found");
    }

    /** Test which checks that method will not return departures with empty destination string. */
    @Test
    @DisplayName("Testing train departures after empty destination input")
    void testTrainsFromDestinationEmptyString() {
      assertThrows(
          IllegalArgumentException.class,
          () -> testTrainDepartureRegister.getTrainDeparturesFromDestination(""),
          "Failure");
    }

    /**
     * Test which checks that method for updating the time will also remove any trains after taking
     * into account delay.
     */
    @Test
    @DisplayName("Testing if changing the time removes departed trains")
    void testRemovedAfterNewTime() {
      // Arrange

      testTrainDepartureRegister.addMinuteDelayToTrainDeparture(223, Duration.ofMinutes(200));
      // Act
      testTrainDepartureRegister.updateCurrentTime(LocalTime.of(8, 30));
      // Assert
      assertFalse(testTrainDepartureRegister.trainExists(222), "Found delayed train");
      assertTrue(
          testTrainDepartureRegister.trainExists(trainNumber), "Found correct non-delayed train");
      assertTrue(
          testTrainDepartureRegister.trainExists(223), "Did not find correct non-delayed train");
    }

    /** Tests for method which sorts train registry by trains' departure time. */
    @Nested
    @DisplayName("Register sorting test")
    public class RegisterTrainSorting {
      @Test
      @DisplayName("Tests for successful sorting by departure time")
      void testGetTrainDepartureHashMap() {
        HashMap<Integer, TrainDeparture> wantSorted =
            testTrainDepartureRegister.getTrainDepartureHashMap();
        TrainDepartureRegister manuallySortedRegister = new TrainDepartureRegister();
        manuallySortedRegister.addTrainDeparture(newTestDeparture1, 222);
        manuallySortedRegister.addTrainDeparture(newTestDeparture2, 223);
        manuallySortedRegister.addTrainDeparture(newTestDeparture, trainNumber);
        assertEquals(manuallySortedRegister.getTrainDepartureHashMap(), wantSorted);
      }
    }

    /** Tests for methods related to currentTime field in TrainDepartureRegistry class. */
    @Nested
    @DisplayName("Test methods affecting currentTime")
    class TestCurrentTime {

      /** Test for getCurrentTime method successfully getting current time. */
      @Test
      @DisplayName("Test for successfully getting current time")
      void testCurrentTimeSuccess() {
        LocalTime testTime = LocalTime.of(23, 23);
        testTrainDepartureRegister.updateCurrentTime(testTime);
        assertEquals(
            testTime, testTrainDepartureRegister.getCurrentTime(), "Wrong time from getter");
      }

      /** Test for updating currentTime with valid input parameter. */
      @Test
      @DisplayName("Positive test for updating currentTime with valid parameter")
      void testUpdateCurrentTimeNewer() {
        LocalTime testTime = LocalTime.of(12, 0);
        assertDoesNotThrow(
            () -> {
              testTrainDepartureRegister.updateCurrentTime(testTime);
              assertEquals(
                  testTime, testTrainDepartureRegister.getCurrentTime(), "Time was not updated");
            });
      }

      /** Test to check that updateCurrentTime does not update with earlier time. */
      @Test
      @DisplayName("Test that time is only updated with increased time")
      void testUpdateCurrentTimeOlder() {
        // Arrange
        LocalTime testTime = LocalTime.of(12, 0);
        testTrainDepartureRegister.updateCurrentTime(testTime);
        // Act
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () -> testTrainDepartureRegister.updateCurrentTime(LocalTime.of(6, 0)),
                // Assert
                "Current time was updated to earlier time");
        assertEquals("New time should be after current time", e.getMessage());
      }

      /** Test that updateCurrentTime does not accept null parameter input. */
      @Test
      @DisplayName("Test that updateCurrentTime does not update with null input")
      void testUpdateCurrentTimeNull() {
        // Act
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () -> testTrainDepartureRegister.updateCurrentTime(null),
                // Assert
                "Current time was erroneously updated to null");
        assertEquals("Should not take null as parameter", e.getMessage());
      }
    }

    /** Positive tests for setTrackFromTrainNumber. */
    @Nested
    @DisplayName("Positive tests for setTrackFromTrainNumber")
    class TestSetTrackFromTrainNumber {
      /** Positive test for setTrackFromTrainNumber correctly setting a positive train number. */
      @Test
      @DisplayName("Test that setTrackFromTrainNumber correctly sets positive track number")
      void testSetTrackFromTrainNumberPositive() {

        testTrainDepartureRegister.setTrackFromTrainNumber(trainNumber, 1);
        assertEquals(
            1,
            testTrainDepartureRegister.getTrainFromTrainNumber(trainNumber).getTrack(),
            "Track set correctly");
      }

      /** Positive test for setTrackFromTrainNumber correctly setting a positive train number. */
      @Test
      @DisplayName("Test that setTrackFromTrainNumber correctly sets track number -1")
      void testSetTrackFromTrainNumberMinusOne() {

        testTrainDepartureRegister.setTrackFromTrainNumber(trainNumber, -1);
        assertEquals(
            -1,
            testTrainDepartureRegister.getTrainFromTrainNumber(trainNumber).getTrack(),
            "Track set correctly");
      }
    }

    /** Tests for printHashMap */
    @Nested
    @DisplayName("Tests for printHashMap")
    class TestPrintHashMap {
      /** Test that printHashMap() prints the desired output. */
      @Test
      @DisplayName("Tests that printHashMap prints correctly")
      void testPrintHashMapPrintout() {
        String testString =
            """
                   06:00           G5        222          Trondheim                       5             06:00

                   06:01           G5        223          Trondheim                       6             06:01

                   12:00           G5        777          Oslo                            3             12:00

                   """;
        testTrainDepartureRegister.printHashMap(
            testTrainDepartureRegister.getTrainDepartureHashMap());
        assertEquals(testString, outputStreamCaptor.toString());
      }
    }

    /** Tests for exceptions being thrown at the right time. */
    @Nested
    @DisplayName("Tests for throwing exceptions")
    class IllegalArgumentTests {
      /** Test that testTrainExists throws exceptions for negative train number. */
      @Test
      @DisplayName("Test that you cannot verify a train with negative train number")
      void testTrainExistsNegativeTrainNumberException() {
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () -> testTrainDepartureRegister.trainExists(-5),
                "Should throw exception with negative input");
        assertEquals("Please give a positive int for Train Number", e.getMessage());
      }

      /** Test that addMinuteDelayToTrainDeparture throws exceptions when parameter is negative. */
      @Test
      @DisplayName("Tests that you cannot add negative duration")
      void addMinuteDelayToTrainDepartureNegativeException() {
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () ->
                    testTrainDepartureRegister.addMinuteDelayToTrainDeparture(
                        trainNumber, Duration.ofMinutes(-5)),
                "Should throw exception with negative input");
        assertEquals("Non-positive duration", e.getMessage());
      }

      /** Test that setTrackFromTrainNumber throws exceptions if the track-number is zero. */
      @Test
      @DisplayName("Test setTrackFromTrainNumber for exceptions with 0")
      void setTrackFromTrainNumberZeroTrack() {
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () -> testTrainDepartureRegister.setTrackFromTrainNumber(trainNumber, 0),
                "Should throw exception with input 0");
        assertEquals("Please give a positive int or -1 for Train Track", e.getMessage());
      }

      /** Test that setTrackFromTrainNumber throws exceptions if the track-number is below -1. */
      @Test
      @DisplayName("Test setTrackFromTrainNumber for exceptions with -2")
      void setTrackFromTrainNumberNegative() {
        IllegalArgumentException e =
            assertThrows(
                IllegalArgumentException.class,
                () -> testTrainDepartureRegister.setTrackFromTrainNumber(trainNumber, -2),
                "Should throw exception with input -2");
        assertEquals("Please give a positive int or -1 for Train Track", e.getMessage());
      }
    }
  }
}
