package edu.ntnu.stud.kandidat.launcher;

import edu.ntnu.stud.kandidat.view.UserInterface;

/**
 * This is the main class for the train dispatch application. Launches the user interface
 *
 * @author 10016
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {
  /**
   * The application itself runs here.
   *
   * @param args is the standard argument for the main method
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    userInterface.start();
    userInterface.init();
  }
}
