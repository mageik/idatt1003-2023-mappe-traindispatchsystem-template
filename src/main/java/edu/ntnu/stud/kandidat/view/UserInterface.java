package edu.ntnu.stud.kandidat.view;

import edu.ntnu.stud.kandidat.register.TrainDeparture;
import edu.ntnu.stud.kandidat.register.TrainDepartureRegister;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class for the user interface.
 *
 * @author mageik
 * @version 1.0
 * @since 0.1
 */
public class UserInterface {
  private static final int PRINT_SORTED_LIST = 1;
  private static final int ADD_TRAIN = 2;

  private static final int FIND_TRAIN = 3;
  private static final int ADD_TRAIN_DELAY = 4;
  private static final int ASSIGN_TRAIN_TRACK = 5;
  private static final int SEARCH_TRAIN_DESTINATION = 6;
  private static final int UPDATE_TIME = 7;

  private static final int EXIT = 8;
  private final Scanner sc1;
  private final TrainDepartureRegister trainRegister1;

  /** Constructor creates a scanner and a TrainDepartureRegister. */
  public UserInterface() {
    sc1 = new Scanner(System.in);
    trainRegister1 = new TrainDepartureRegister();
  }

  /**
   * Initializes the application at the start with default values.
   *
   * @author mageik
   * @since 0.1
   */
  public void start() {

    trainRegister1.addTrainDepartureFromInput(LocalTime.of(12, 15), "Z3", "Oslo", 15, 200);
    trainRegister1.addTrainDepartureFromInput(LocalTime.of(10, 15), "G5", "Bergen", 16, 205);
    trainRegister1.addTrainDepartureFromInput(LocalTime.of(21, 15), "A8", "Bergen", 16, 203);
    trainRegister1.addTrainDepartureFromInput(LocalTime.of(6, 15), "B1", "Bergen", 1, 207);
  }

  /**
   * Starts and runs the main UI the user will be using.
   *
   * @author 10016 version 1.0
   * @since 0.1
   */
  public void init() {
    boolean finished = false;

    while (!finished) {

      int menuChoice = this.showMenu();
      switch (menuChoice) {
        case PRINT_SORTED_LIST -> printSortedListMenu();

        case ADD_TRAIN -> addTrainMenu();

        case FIND_TRAIN -> findTrainMenu();

        case ADD_TRAIN_DELAY -> addTrainDelayMenu();

        case ASSIGN_TRAIN_TRACK -> assignTrainTrackMenu();

        case SEARCH_TRAIN_DESTINATION -> searchDestinationMenu();

        case UPDATE_TIME -> updateTimeMenu();

        case EXIT -> finished = true;
        default -> System.out.println("Error: Invalid menu selection.");
      }
    }
  }

  /**
   * Presents the menu for the user, and awaits input from the user. The menu * choice selected by
   * the user is being returned.
   *
   * @return the menu choice by the user as a positive number starting from 1. * If 0 is returned,
   *     the user has entered a wrong value
   */
  private int showMenu() {

    int menuChoice = 0;
    System.out.println("\n***** Train Registry *****\n");
    System.out.println("1. View departure table");
    System.out.println("2. Add train with unique train number");
    System.out.println("3. Find train based on train number");
    System.out.println("4. Add train delay");
    System.out.println("5. Assign train track");
    System.out.println("6. Search for train(s) based on train destination");
    System.out.println("7. Set time");
    System.out.println("8. Quit");
    System.out.println("\nPlease enter a number between 1 and 8.\n");
    Scanner sc2 = new Scanner(System.in);

    if (sc2.hasNextInt()) {
      menuChoice = sc2.nextInt();

    } else {
      System.out.println("You must enter a number, not text");
    }
    return menuChoice;
  }

  /** Method to print a header for the departure table in the user interface. */
  private void printSortedListMenu() {
    System.out.println("Current Time: " + trainRegister1.getCurrentTime());
    System.out.println("Destinations:");
    System.out.println(
        "Departure Time | Line | Train Number | Destination |"
            + "    Delay    | Track Number |  New Departure Time");

    trainRegister1.printHashMap(trainRegister1.getTrainDepartureHashMap());
  }

  /**
   * Presents a menu for adding trains. Asks for relevant parameters for {@link TrainDeparture}
   * constructor plus train number. Asks questions again if invalid input is provided.
   */
  private void addTrainMenu() {

    String lineName = "";
    String destination = "";
    LocalTime trainDepartureTime = null;
    int trainNumber1 = 0;

    int trainTrack = 0;
    boolean answered = false;

    do {
      System.out.println("Give the train departure time in format HH:mm: ");
      String inputTime = sc1.nextLine();
      try {
        trainDepartureTime = LocalTime.parse(inputTime, DateTimeFormatter.ofPattern("HH:mm"));
        answered = true;
      } catch (DateTimeParseException e) {
        System.out.println("Invalid time");
      }
    } while (!answered);
    answered = false;
    do {
      System.out.println("Give the train line of the route: ");
      try {

        lineName = sc1.nextLine();
        if (!lineName.isBlank()) {
          answered = true;
        }
      } catch (IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    answered = false;
    do {
      System.out.println("Give unique train number: ");
      try {
        trainNumber1 = sc1.nextInt();
        sc1.nextLine();
        if (trainNumber1 > 0) {
          if (!trainRegister1.trainExists(trainNumber1)) {
            answered = true;
          }
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    answered = false;
    do {
      System.out.println("Give train destination: ");
      try {
        destination = sc1.nextLine();
        if (!destination.isBlank()) {
          answered = true;
        }
      } catch (IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    answered = false;
    do {

      System.out.println("Give train track (Provide -1 if there is no decided train track): ");
      try {
        trainTrack = sc1.nextInt();
        sc1.nextLine();
        if ((trainTrack > 0) || (trainTrack == -1)) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    try {

      trainRegister1.addTrainDepartureFromInput(
          trainDepartureTime, lineName, destination, trainTrack, trainNumber1);
      System.out.println("New train added");

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      System.out.println("Train not added");
    }
  }

  /**
   * Menu for returning a train departure from train numbers. Asks question again if input is
   * invalid.
   */
  private void findTrainMenu() {
    boolean answered = false;
    do {
      System.out.println("Provide valid train number: ");
      try {
        int trainNumber1 = sc1.nextInt();
        sc1.nextLine();

        if ((trainNumber1 > 0) && (trainRegister1.trainExists(trainNumber1))) {

          System.out.println(
              "Departure Time | Line | Train Number | Destination "
                  + "|    Delay    | Track Number |  New Departure Time");
          System.out.println(
              trainRegister1
                  .getTrainFromTrainNumber(trainNumber1)
                  .printTrainDeparture(trainNumber1));
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
  }

  /**
   * Menu for adding delay to train departure based on train number. Asks questions again if input
   * is invalid.
   */
  private void addTrainDelayMenu() {
    int trainNumber1 = 0;
    boolean answered = false;
    do {
      System.out.println("Provide the train number for the delayed train: ");
      try {
        trainNumber1 = sc1.nextInt();
        sc1.nextLine();
        if ((trainNumber1 > 0) && (trainRegister1.trainExists(trainNumber1))) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    int trainDelay;
    answered = false;
    do {
      System.out.println("Please provide the delay in minutes: ");
      try {
        trainDelay = sc1.nextInt();
        sc1.nextLine();
        if (trainDelay > 0) {
          Duration additionalDelay = Duration.ofMinutes(trainDelay);
          trainRegister1.addMinuteDelayToTrainDeparture(trainNumber1, additionalDelay);
          answered = true;
        }

      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
  }

  /**
   * Menu which assigns a track to a train departure based on train number. Asks questions again if
   * input is invalid.
   */
  private void assignTrainTrackMenu() {
    int trainNumber1 = 0;
    int trainTrack1 = 0;
    boolean answered = false;
    do {
      System.out.println("Provide the train number for the train you wish to assign a track: ");
      try {
        trainNumber1 = sc1.nextInt();
        sc1.nextLine();
        if (trainNumber1 > 0) {
          if (trainRegister1.trainExists(trainNumber1)) {
            answered = true;
          } else {
            System.out.println("Please provide the train number of a registered train.");
          }
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    answered = false;
    do {
      System.out.println("Provide the new train track number: ");
      try {
        trainTrack1 = sc1.nextInt();
        sc1.nextLine();
        if (trainTrack1 > 0) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
    trainRegister1.setTrackFromTrainNumber(trainNumber1, trainTrack1);
  }

  /**
   * Menu which returns train departures with input destination. Asks question again if destination
   * is invalid.
   */
  private void searchDestinationMenu() {
    String trainDestination1;
    boolean answered = false;
    do {
      System.out.println("Provide the destination for the trains: ");
      try {
        trainDestination1 = sc1.nextLine();
        if (!trainDestination1.isBlank()) {
          System.out.println(
              "Departure Time | Line | Train Number | Destination "
                  + "|    Delay    | Track Number |  New Departure Time");
          trainRegister1.printHashMap(
              trainRegister1.getTrainDeparturesFromDestination(trainDestination1));
          answered = true;
        }
      } catch (IllegalArgumentException e) {
        sc1.nextLine();
      }
    } while (!answered);
  }

  /** Manu which updates the current time. Does not accept invalid input. */
  private void updateTimeMenu() {
    LocalTime newTime;
    boolean answered = false;
    do {
      System.out.println("Current time is: " + trainRegister1.getCurrentTime());
      System.out.println("Please provide the newer, updated time in format HH:mm : ");
      try {

        newTime = LocalTime.parse(sc1.nextLine(), DateTimeFormatter.ofPattern("HH:mm"));

        trainRegister1.updateCurrentTime(newTime);
        System.out.println("The time is now: " + trainRegister1.getCurrentTime());
        answered = true;

      } catch (DateTimeParseException | IllegalArgumentException e) {
        System.out.println("Time is invalid or earlier than current time. Time not updated.");
      }
    } while (!answered);
  }
}
