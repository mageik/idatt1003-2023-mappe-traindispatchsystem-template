package edu.ntnu.stud.kandidat.register;

import java.time.Duration;
import java.time.LocalTime;

/**
 * This class describes train departures from the train station. Contains a constructor to create a
 * train departure with the necessary attributes. Uses setters in constructor to verify input
 * parameters, and throws exceptions for illegal data. Setters are private when not needed by
 * outside actors.
 *
 * @author 10016
 * @version 1.0
 * @since 0.1
 */
public class TrainDeparture {
  private final Verification verification = new Verification();
  private String line;
  private String destination;
  private LocalTime originalDepartureTime;
  private int track;
  private Duration minuteDelay;

  /**
   * Constructor for TrainDeparture.
   *
   * @param originalDepartureTime The departure time before any potential delays.
   * @param line The train's line value
   * @param destination The Train's destination
   * @param track The train's designated track
   * @throws IllegalArgumentException Throws if parameters are invalid. Checks for: -
   *     originalDepartureTime not null - line not blank - destination not blank - track number is
   *     positive or -1.
   */
  public TrainDeparture(
      LocalTime originalDepartureTime, String line, String destination, int track) {
    setOriginalDepartureTime(originalDepartureTime);
    setLine(line);
    setDestination(destination);
    setTrack(track);
    this.minuteDelay = Duration.ofMinutes(0);
  }

  /**
   * Return a LocalTime representing the effective departure time accounting for delays.
   *
   * @return LocalTime with departureTime after delay.
   */
  public LocalTime getDepartureTime() {
    return originalDepartureTime.plusMinutes(minuteDelay.toMinutes());
  }

  /**
   * Return a LocalTime representing the original departure time.
   *
   * @return LocalTime with originalDepartureTime
   */
  public LocalTime getOriginalDepartureTime() {
    return originalDepartureTime;
  }

  /**
   * Private setter method for original departure time which verifies parameter is non-null.
   *
   * @param localTime LocalTime parameter to be verified non-null.
   * @throws IllegalArgumentException if verification fails due to null input.
   */
  private void setOriginalDepartureTime(LocalTime localTime) {
    verification.verifyLocalTime(localTime);
    this.originalDepartureTime = localTime;
  }

  /**
   * Return a String with the designated line.
   *
   * @return String with the line
   */
  public String getLine() {
    return line;
  }

  /**
   * Private setter method for line which verifies that input is not empty.
   *
   * @param line String parameter to be verified as having real content.
   * @throws IllegalArgumentException if verification fails due to blank input.
   */
  private void setLine(String line) {
    verification.verifyString(line, "Train Line");
    this.line = line;
  }

  /**
   * Returns a String with the destination.
   *
   * @return String destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Private setter method for destination which verifies that input is not empty.
   *
   * @param destination String parameter to be verified as having real content.
   * @throws IllegalArgumentException if verification fails due to blank input.
   */
  private void setDestination(String destination) {
    verification.verifyString(destination, "Destination");
    this.destination = destination;
  }

  /**
   * Returns the Duration representing the delay in minutes.
   *
   * @return A Duration minuteDelay.
   */
  public Duration getMinuteDelay() {
    return minuteDelay;
  }

  /**
   * Returns an int for the track number.
   *
   * @return The int track.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Public setter method for train track. Verifies a positive or -1 track.
   *
   * @param track int parameter to be assigned to track
   * @throws IllegalArgumentException if verification fails due to input not positive or -1.
   */
  public void setTrack(int track) {
    verification.verifyPositiveOrNotMinusOneInt(track);
    this.track = track;
  }

  /**
   * Uses verifyPositiveDuration to ensure a duration is positive. Adds duration to previous delay
   * minuteDelay if duration is positive. Will otherwise throw exception.
   *
   * @param addedMinutes The duration to be added.
   * @throws IllegalArgumentException If input is negative.
   */
  public void addMinuteDelay(Duration addedMinutes) {
    verification.verifyPositiveDuration(addedMinutes);

    this.minuteDelay = this.minuteDelay.plusMinutes(addedMinutes.toMinutes());
  }

  /**
   * Method is used to provide a string for train departure information to the {@link
   * edu.ntnu.stud.kandidat.view.UserInterface}.
   *
   * @param trainNumber Needs input of train number, since this is stored in the HashMap in {@link
   *     TrainDepartureRegister}.
   * @return Formatted String output of departure time, line, train number, destination, possible
   *     delay, track, and delayed departure time.
   */
  public String printTrainDeparture(int trainNumber) {
    return String.format(
        "%-15s %-9s %-12s %-15s %-15s %-13d %-2s%n",
        originalDepartureTime,
        line,
        trainNumber,
        destination,
        (minuteDelay.isZero()
            ? ""
            : (minuteDelay.toMinutes() / 60 + "h, " + minuteDelay.toMinutes() % 60 + "m")),
        track,
        this.originalDepartureTime.plusMinutes(minuteDelay.toMinutes()));
  }
}
