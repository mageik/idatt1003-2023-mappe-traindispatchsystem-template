package edu.ntnu.stud.kandidat.register;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class used to make a register of multiple train departures. Constructor does not need input.
 * Registers start out empty at time 00:00.
 *
 * @author 10016
 * @version 1.0
 * @since 0.2
 */
public class TrainDepartureRegister {
  private final Verification verification = new Verification();

  /**
   * HashMap used to create a collection of multiple objects of class TrainDeparture. HashMap is
   * malleable with useful built-in methods. HashMap has unique value for each included
   * TrainDeparture. We have a unique train number associated with each TrainDeparture object.
   */
  private HashMap<Integer, TrainDeparture> trainDepartureHashMap;

  /** LocalTime used to compare current time with train departure time. */
  private LocalTime currentTime;

  /** Constructor which creates an empty ArrayList for objects of class TrainDeparture. */
  public TrainDepartureRegister() {
    this.trainDepartureHashMap = new HashMap<>();
    this.currentTime = LocalTime.of(0, 0);
  }

  /**
   * Method to search for instances of a trainDeparture with identical train number already found in
   * an ArrayList.
   *
   * @param trainNumber The train number to check for
   * @return boolean for if trainDeparture is already found
   */
  public boolean trainExists(int trainNumber) {
    verification.verifyPositiveInt(trainNumber);

    return trainDepartureHashMap.containsKey(trainNumber);
  }

  /**
   * TrainDeparture method which takes in int trainNumber and returns the TrainDeparture train which
   * has trainNumber as its trainNumber. Verifies positive trainNumber using verifyPositiveInt
   *
   * @param trainNumber An int for the trainNumber the user want the associated train of.
   * @return TrainDeparture train with input trainNumber as its trainNumber.
   * @throws IllegalArgumentException using verifyPositiveInt if trainNumber is negative.
   */
  public TrainDeparture getTrainFromTrainNumber(int trainNumber) {
    verification.verifyPositiveInt(trainNumber);
    return trainDepartureHashMap.get(trainNumber);
  }

  /**
   * Returns HashMap that has been reordered.
   *
   * @return HashMap reordered HashMap
   */
  public HashMap<Integer, TrainDeparture> getTrainDepartureHashMap() {
    this.reorderHashMap();

    return trainDepartureHashMap;
  }

  /**
   * Method which returns a new HashMap containing only those with the correct destination. Verifies
   * that the destination parameter is not blank. Throws exceptions if it is. Returned HashMap is
   * sorted by original departure time.
   *
   * @param destination String input with the destination to be searched for.
   * @return HashMap containing only the pairs of trainNumber and TrainDeparture with the correct
   *     destination.
   * @throws IllegalArgumentException using verifyString if the input String destination is blank.
   */
  public HashMap<Integer, TrainDeparture> getTrainDeparturesFromDestination(String destination) {
    verification.verifyString(destination, "Destination");
    HashMap<Integer, TrainDeparture> tempHashMap = new HashMap<>(this.trainDepartureHashMap);
    tempHashMap
        .entrySet()
        .removeIf(
            integerTrainDepartureEntry ->
                !integerTrainDepartureEntry.getValue().getDestination().equals(destination));

    tempHashMap =
        tempHashMap.entrySet().stream()
            .sorted(Comparator.comparing(entry -> entry.getValue().getDepartureTime()))
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey,
                    Map.Entry::getValue,
                    (existing, replacement) -> existing,
                    LinkedHashMap::new));
    return tempHashMap;
  }

  /**
   * Method to add a TrainDeparture with trainNumber to the HashMap trainDepartureHashMap. Throws
   * exceptions if a TrainDeparture with the same trainNumber is already in the HashMap. Used for
   * testing and in addTrainDepartureFromInput. Is public due to use in testing.
   *
   * @param trainDeparture An object of class TrainDeparture. Uses trainExist to check if it already
   *     exists in the HashMap.
   * @throws IllegalArgumentException If trainExists finds a TrainDeparture with the same
   *     trainNumber
   */
  public void addTrainDeparture(TrainDeparture trainDeparture, int trainNumber)
      throws IllegalArgumentException {
    verification.verifyPositiveInt(trainNumber);
    if (!this.trainExists(trainNumber)) {
      trainDepartureHashMap.put(trainNumber, trainDeparture);

    } else {
      throw new IllegalArgumentException("Train number already in use");
    }
  }

  /**
   * Method which creates a new TrainDeparture in the trainDepartureHashMap. Throws exceptions if
   * train number is already used. Reduces coupling between {@link
   * edu.ntnu.stud.kandidat.view.UserInterface} and {@link TrainDeparture}.
   *
   * @param inputTime The LocalTime to be assigned to the train.
   * @param lineInput The line to be assigned to the train.
   * @param destinationInput The destination to be assigned to the train.
   * @param track The in track for the train.
   * @param trainNumber The int trainNumber used as unique Key in trainDepartureHashMap.
   * @throws IllegalArgumentException Throws exception if trainNumber is already a Key in the
   *     HashMap
   */
  public void addTrainDepartureFromInput(
      LocalTime inputTime, String lineInput, String destinationInput, int track, int trainNumber) {
    TrainDeparture tempTrain = new TrainDeparture(inputTime, lineInput, destinationInput, track);
    addTrainDeparture(tempTrain, trainNumber);
  }

  /**
   * Method to print content of input parameter map.
   *
   * @param map HashMap to be printed.
   */
  public void printHashMap(HashMap<Integer, TrainDeparture> map) {
    map.forEach((key, value) -> System.out.println(value.printTrainDeparture(key)));
  }

  /**
   * Method that uses filter to sort a HashMap according to originalDepartureTime and then return
   * it.
   */
  private void reorderHashMap() {
    this.trainDepartureHashMap =
        this.trainDepartureHashMap.entrySet().stream()
            .sorted(Comparator.comparing(entry -> entry.getValue().getOriginalDepartureTime()))
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey,
                    Map.Entry::getValue,
                    (existing, replacement) -> existing,
                    LinkedHashMap::new));
  }

  /**
   * Method which removes train departures which have a departure time after the current time.
   * Method uses getDepartureTime() to take into account delays.
   */
  private void removeDepartedTrainDepartures() {
    trainDepartureHashMap
        .entrySet()
        .removeIf(train -> train.getValue().getDepartureTime().isBefore(this.currentTime));
  }

  /**
   * Adds a delay in minutes to the departureTime of the object of class trainDeparture with
   * trainNumber as its train number.
   *
   * @param trainNumber the int trainNumber
   * @param delay long delay in minutes
   */
  public void addMinuteDelayToTrainDeparture(int trainNumber, Duration delay) {
    verification.verifyPositiveInt(trainNumber);
    verification.verifyPositiveDuration(delay);
    trainDepartureHashMap.get(trainNumber).addMinuteDelay(delay);
  }

  /**
   * Standard getter-method for LocalTime currentTime field.
   *
   * @return LocalTime currentTime
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Method to update LocalTime if the new time is after the current time.
   *
   * @param time Is set as the new time if it after current time.
   */
  public void updateCurrentTime(LocalTime time) throws IllegalArgumentException {
    if (time == null) {
      throw new IllegalArgumentException("Should not take null as parameter");
    } else if (!time.isAfter(this.currentTime)) {
      throw new IllegalArgumentException("New time should be after current time");

    } else {
      this.currentTime = time;
      removeDepartedTrainDepartures();
    }
  }

  /**
   * Setter method for TrainDepartureRegister to reduce coupling.
   *
   * @param trainNumber The int train number of the train which should change track number.
   * @param track The new int track number for the train departure.
   */
  public void setTrackFromTrainNumber(int trainNumber, int track) {
    trainDepartureHashMap.get(trainNumber).setTrack(track);
  }
}
