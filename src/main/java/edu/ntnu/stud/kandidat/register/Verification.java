package edu.ntnu.stud.kandidat.register;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Class to do verification in other classes.
 *
 * @author 10016
 * @version 1.0
 * @since 1.0
 */
public class Verification {

  /** Empty constructor. */
  public Verification() {}

  /**
   * Verifies that the input string isn't empty or spaces.
   *
   * @param inputString the String that needs to not be empty
   * @param fieldName which field is being tested
   * @throws IllegalArgumentException Throws exception if the input string is empty "" or spaces.
   *     Used for line and destination.
   */
  public void verifyString(String inputString, String fieldName) throws IllegalArgumentException {
    if (inputString.isBlank()) {
      throw new IllegalArgumentException(fieldName + " is an empty string");
    }
  }

  /**
   * Verifies that the input integer is positive or -1.
   *
   * @param numberToVerify the int that needs to be verified as a positive integer or -1.
   * @throws IllegalArgumentException the int values need to be positive or -1. Throws exception if
   *     the input zero or less than 1.
   */
  public void verifyPositiveOrNotMinusOneInt(int numberToVerify) throws IllegalArgumentException {
    if (numberToVerify == 0) {
      throw new IllegalArgumentException("Please give a positive int or -1 for Train Track");
    } else if (numberToVerify < -1) {
      throw new IllegalArgumentException("Please give a positive int or -1 for Train Track");
    }
  }

  /**
   * Verifies that the input LocalTime has a value other than null.
   *
   * @param timeToTest The LocalTime to be tested
   * @throws IllegalArgumentException Throws exception if LocalTime is null.
   */
  public void verifyLocalTime(LocalTime timeToTest) throws IllegalArgumentException {
    if (timeToTest == null) {
      throw new IllegalArgumentException("Invalid time input. Time cannot be null");
    }
  }

  /**
   * Method to verify that duration input is positive.
   *
   * @param duration Input duration to be tested.
   * @throws IllegalArgumentException Throws exception if duration is non-positive.
   */
  public void verifyPositiveDuration(Duration duration) throws IllegalArgumentException {
    if (duration.isNegative()) {
      throw new IllegalArgumentException("Non-positive duration");
    }
  }

  /**
   * Verifies that the input integer is positive.
   *
   * @param numberToVerify the int that needs to be verified as a positive integer
   * @throws IllegalArgumentException the int values need to be positive. Throws exception if the
   *     input is not a negative int. Used for track and delay
   */
  public void verifyPositiveInt(int numberToVerify) throws IllegalArgumentException {
    if (numberToVerify < 0) {
      throw new IllegalArgumentException("Please give a positive int for Train Number");
    }
  }
}
