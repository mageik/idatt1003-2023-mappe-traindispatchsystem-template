# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Magnus Eik"  
STUDENT ID = "10016"

## Project description

[//]: # This project is for the IDATT1003 exam exercise. The program should describe a train dispatch system. The user should be able to
use a user interface to see a table of expected train departures, add new train departures, find trains based on train number, add delays to trains, assign a track to a train, and search for trains with the same destination, and set the time of day. The program is split into TrainDispatchApp, TrainDeparture, TrainDepartureRegister and UserInterface classes. TrainDispatchApp runs the program, UserInterface creates the userinterface, TrainDeparture describes a single instance of a train departure, TrainDepartureRegister keeps track of a collection of multiple train departure objects for a specific point in time.

## Project structure

[//]: # The files are in the src/main/java/edu/ntnu/stud/kandidat folder.

## Link to repository

[//]: # https://gitlab.stud.idi.ntnu.no/mageik/idatt1003-2023-mappe-traindispatchsystem-template

## How to run the project

[//]: # The program runs from the TrainDispatchApp file.

## How to run the tests

[//]: # The test files are in the test/java/edu/ntnu/stud/kandidat folder.

